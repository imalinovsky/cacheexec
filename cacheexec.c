#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ALLOCATION_MEGABYTES 2 

static void thrash_memory(char *p, const size_t size){
        const size_t half_size = size / 2;
	unsigned i = 1;

        while (i) {
                const size_t copy_size = 1 * 1024;
                const int rnd1 = rand();
                const int rnd2 = rand();
                const size_t si = half_size + rnd1 % (half_size - copy_size);
                const size_t di = rnd2 % (half_size - copy_size);
                memcpy(&p[di], &p[si], copy_size);
        }
}


int main(){
	int allocation_size = ALLOCATION_MEGABYTES * 1024 * 1024;
	char *thrash = malloc(allocation_size);
		thrash_memory((char *)thrash, allocation_size);
	return 0;
}
