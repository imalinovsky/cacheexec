/* Protect few cache lines from being evicted by allocating, reading into it, 
 * then removing from everybody's allocation.
 * To compile
 * 1. scl enable devtoolset-3 /bin/bash
 * 2. gcc <filename> -o <executable> -lpqos -lhugetlbfs -W
 *
 * Before running the test
 * 1. create COS1 with 4 ways and COS2 with the rest  pqos -e 'llc:1=0xf;llc:2=0xffff0' -a 'llc:1=9;llc:2=0-8'
 * 2. Configure huge pages echo 128 > /sys/devices/system/node/node0/hugepages/hugepages-2048kB/nr_hugepages
 *
 * To run your executable:
 * 1. setting COS is a privileged instruction. You would have to use a wrapper for that. Ion has created one 
 * 2. Using the wrapper run "sudo taskset -c 9 ./rawio-wrapper <your executable>"
 *
 * Example of how to protect cache lines:
 *
 * 	#define _GNU_SOURCE
 *
 * 	#include <stdlib.h>
 *  	#include <stdio.h>
 *  	#include <string.h>
 *	#include "cachelock.h"
 *
 * 	#define UNPROTECTED_COS 2
 * 	#define PROTECTED_COS 1
 * 	#define ALLOCATION_MEGABYTES 2 
 * 	#define HUGEPAGES_ENABLED 1 // 0 - don't use hugepages
 * 	#define LINESIZE 64
 *
 *
 * 	int main(){
 * 	        int allocation_size = ALLOCATION_MEGABYTES * 1024 * 1024;
 *		char *protected = protected_malloc (allocation_size, PROTECTED_CORE, PROTECTED_COS, UNPROTECTED_COS, HUGEPAGES_ENABLED, LINESIZE );
 *	   	
 *	   	//Write something into the protected memory 
 *		for (i = 0; i < allocation_size; ++i ) protected[i] = (char) rand();
 *	{
 *
 *You can also have a glance at Intel's Pseudolock code at https://github.com/01org/intel-cmt-cat/blob/master/examples/c/PSEUDO_LOCK
*
*/
#include <sched.h> 
#include <pqos.h>
#include <sys/mman.h>

/* Initialize PQOS*/
void initalize_pqos (){
        struct pqos_config cfg;
	memset(&cfg, 0, sizeof(cfg));
	if (pqos_init(&cfg) != PQOS_RETVAL_OK) printf ("Error initializing PQoS library!\n");
}


/*Change COS allocation to COS value*/
int set_cat_cos (int CORE, int COS){
        if (pqos_alloc_assoc_set(CORE, COS) != PQOS_RETVAL_OK) printf("Setting allocation class of service association failed!\n");
}

/*Alligned memory allocation*/
void *allocate(int linesize, unsigned int allocation_size, int hugepages_enabled){
        unsigned int i;
	void *p;
        if (hugepages_enabled)
 		p = mmap (NULL, allocation_size, PROT_READ | PROT_WRITE,
						 MAP_PRIVATE | MAP_ANONYMOUS |MAP_POPULATE | MAP_HUGETLB, -1,0);
        else
                posix_memalign((void **)&p, linesize, allocation_size);
	mem_flush(p, allocation_size);

	//Flushing few times improves the lock. Unclear why
	for (i = 0; i < 3; i++) mem_flush(p, allocation_size);
	//Read multiple times to minimize the impact of cache evictions while reading
        for (i = 0; i < 10; i++) mem_read (p,allocation_size);
        
        return p;
}

/*Pinning the current process to a core*/
static int get_affinity (){
        cpu_set_t  mask;
        CPU_ZERO (&mask);
	int i;
        sched_getaffinity (0, sizeof(cpu_set_t), &mask);

	for (i = 0; i < CPU_SETSIZE; i++){
		if (CPU_ISSET (i, &mask)) return i;
	}
}

//Comes from Intel's Pseudolock code https://github.com/01org/intel-cmt-cat/blob/master/examples/c/PSEUDO_LOCK/dlock.c

mem_flush(const void *p, unsigned int allocation_size){
        const size_t cache_line = 64;
        const char *cp = (const char *)p;
        size_t i = 0;

        if (p == NULL || allocation_size <= 0)
                return;

        for (i = 0; i < allocation_size; i += cache_line) {
                asm volatile("clflush (%0)\n\t"
                             : 
                             : "r"(&cp[i])
                             : "memory");
        }

        asm volatile("sfence\n\t"
                     :
                     :
                     : "memory");
}


//Comes from Intel's Pseudolock code https://github.com/01org/intel-cmt-cat/blob/master/examples/c/PSEUDO_LOCK/dlock.c

mem_read(const void *p, unsigned int allocation_size){
        register size_t i;

        for (i = 0; i < (allocation_size/ sizeof(uint32_t)); i++) {
                asm volatile("xor (%0,%1,4), %%eax\n\t"
                             :
                             : "r" (p), "r" (i)
                             : "%eax", "memory");
        }

        for (i = allocation_size & (~(sizeof(uint32_t) - 1)); i < allocation_size; i++) {
                asm volatile("xorb (%0,%1,1), %%al\n\t"
                             :
                             : "r" (p), "r" (i)
                             : "%al", "memory");
        }
}


void *protected_malloc (int allocation_size, 
			int protected_cos, 
			int unprotected_cos, 
			int hugepages_enabled, 
			int linesize){

        initalize_pqos();
	
        /*Pin to a core with a dedicated cache lines */
//        set_affinity (protected_core);
	int protected_core = get_affinity();
	printf ("Protected core: %d\n",protected_core);

        /*Allocate aligned memory to protected */
        if (set_cat_cos(protected_core, protected_cos) == PQOS_RETVAL_OK) printf
                ("Changed core %d allocation to protected COS%d\n",protected_core,protected_cos);

        void *protected = allocate ( linesize, allocation_size, hugepages_enabled);
        if (protected) printf("Protected is allocated %d bytes\n",allocation_size);


        /* Move to unprotected cache lines */
        if (set_cat_cos(protected_core, unprotected_cos) == PQOS_RETVAL_OK) printf
                ("Changed core %d allocation to unportected COS%d\n",protected_core,unprotected_cos);

	return protected;

}

static void cleanup_on_exit (void *p, int allocation_size){
       mem_flush(p, allocation_size);
       free_hugepage_region(p);
}
